No.BSS
=================

Want to create something custom tailored? Want a slim, repsonsive design. Don't want to be another Bootstrapper? Say hello to "No Bootstrap Stylesheets".


What is it?
-----

A modified version of the 960px grid framework Skeleton, to use em's instead of px's. It has also been moved to using less for compiling, as it is easier to quickly change properties of multiple areas of the stylesheet.
It is a minimalist css framework which serves as a starting point to developing your own custom framework. It styles just anough and set's the stage for easy project startups, and then get's out of the way.

Want to use it or modify it?
-----

To use it in your project just include the no-bss.css file and your all set. If you would like to extend, modify or overhaul No.BSS, grab the set of .less files and begin modifying them. This will require a .less compiler
and I suggest getting a copy of the less compiler offered through Node Package Manager.

Hope You Enjoy!
-----
